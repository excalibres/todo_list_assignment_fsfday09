/**
 * Created by jk_87 on 7/4/2016.
 */
//open encansulate
(function(){
    "use strict";
    //declare app
    var toDoApp = angular.module('toDoApp', []);
    //decalare controller
    var toDoController = function($log) {
        var vm = this;
        vm.task ="";
        vm.toDoList =[];

        vm.addTask =function () {
            vm.toDoList.push({
                task: vm.task
            });
            vm.task = "";
        };
    };
    toDoApp.controller("toDoController", ["$log", toDoController]);
})();

